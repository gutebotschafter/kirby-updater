<?php

use Kirby\Http\Header;

use GutebBotschafter\Updater\UpdaterController;

load([
    'GutebBotschafter\\Updater\\UpdaterController' => __DIR__ . "/src/UpdaterController.php",
]);

Kirby::plugin("gb-kirby/updater", [
    "routes" => function ($kirby) {
        return [
            [
                "pattern" => "/gb/updater",
                "action" => function () use (&$kirby) {
                    $request = $kirby->request()->query()->toArray();
                    $configToken = $kirby->option("token");

                    if ($configToken === $request["token"]) {
                        $updater = new UpdaterController($kirby);

                        return $updater->send();
                    }

                    header::forbidden();

                    die('Unauthorized access');
                }
            ]
        ];
    }
]);
