<?php

namespace GutebBotschafter\Updater;

use Kirby\Cms\App;
use Kirby\Http\Response;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use ZipArchive;

/**
 * Class PageController
 * @package GutebBotschafter\PositionCheck
 */
class UpdaterController
{
    /** @var */
    private $kirby;

    private $root = "";

    private $backup = "";

    private $ignoredFiles = [];

    /**
     * Sets the kirby app instance
     *
     * @param App $kirby
     */
    public function __construct(App $kirby)
    {
        $this->kirby = $kirby;

        $this->root = $this->kirby->root() . "/../content";

        $this->backup = $this->kirby->root() . "/../backup";

        $this->ignoredFiles = [".", "..", ".DS_Store"];
    }


    /**
     * Checks if the backup diretory exists
     *
     * @return bool
     */
    public function checkBackupDirectory(): bool
    {
        return !is_dir($this->backup) ? mkdir($this->backup, 0777, true) : true;
    }

    /**
     * Creates the zip files from content folder
     *
     * @param $source
     * @param $destination
     * @return bool
     */
    public function zip($source, $destination): bool
    {
        if (!extension_loaded('zip') || !file_exists($source)) {
            return false;
        }

        $zip = new ZipArchive();
        if (!$zip->open($destination, ZIPARCHIVE::CREATE)) {
            return false;
        }

        $source = str_replace('\\', '/', realpath($source));

        if (is_dir($source) === true) {
            $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);

            foreach ($files as $file) {
                $file = str_replace('\\', '/', $file);

                if (in_array(substr($file, strrpos($file, '/') + 1), $this->ignoredFiles)) {
                    continue;
                }

                $file = realpath($file);

                if (is_dir($file) === true) {
                    $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
                } elseif (is_file($file) === true) {
                    $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
                }
            }
        } elseif (is_file($source) === true) {
            $zip->addFromString(basename($source), file_get_contents($source));
        }

        return $zip->close();
    }

    /**
     * Returns the json
     *
     * @param array $props
     * @return Response
     */
    public function response(array $props): Response
    {
        return new Response(json_encode($props), "application/json", $props["code"]);
    }

    /**
     * @return false|int|Response
     */
    public function send()
    {
        $directory = $this->checkBackupDirectory();

        if ($directory) {
            $filename = $this->backup . "/backup.zip";
            $zip = $this->zip($this->root, $filename);

            if ($zip) {
                header($_SERVER['SERVER_PROTOCOL'] . ' 200 OK');
                header("Content-Type: application/zip");
                header("Content-Transfer-Encoding: Binary");
                header("Content-Length: " . filesize($filename));
                header("Content-Disposition: attachment; filename=\"" . basename($filename) . "\"");

                return readfile($filename);
            }
        }

        $props = [
            "status" => "failed",
            "message" => "Can't create backup",
            "code" => 400
        ];

        return $this->response($props);
    }
}
